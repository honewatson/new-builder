import { Hello } from './hello.js';

export const Greetings = ({ state, actions }) =>
  <div>
    <Hello state={state} actions={actions} />
    And much much more...
  </div>;
