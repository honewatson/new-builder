const transform = require("sucrase").transform;
const rollup = require('rollup');
const rollupNodeResolve = require('rollup-plugin-node-resolve');
const rollupCommonjs = require('rollup-plugin-commonjs');
const rollupBuble = require('rollup-plugin-buble');
const src = "src";
const dist = "build";

const rollupConfig = (inputFile) => {
    return {
        input: inputFile,
        plugins: [
            rollupNodeResolve({
                browser: true,
                main: true
            }),
            rollupCommonjs({ include: 'node_modules/**' }),
            rollupBuble(),
        ],
    }
};

module.exports = {
    *scripts(task, fileData) {
        task.$.log("scripts");
        yield task
            .source(fileData.src)
            .run({ every: false }, function*(file) {
                console.log(file[0]);
                file[0].data = new Buffer(transform(file[0].data.toString('utf8'), {transforms: ["jsx"]}).code);
                task.$.log(`Output: ${dist}/${file[0].dir}/${fileData.src}`);
            })
            .target(`${dist}/src`);
    },
    *vendor(task, fileData) {
        task.$.log("scripts");
        yield task
            .run({every: false},
            function*(file) {
                yield rollup
                    .rollup(rollupConfig(fileData.src))
                    .then(bundle => {
                        return bundle.generate({
                            format: 'es'
                        });
                    })
                    .then(function outputContent(result) {
                        const outputFile = `build/${fileData.src}`;
                        task.$.write(outputFile, result.code);
                    })
                    .catch(e => {
                        task.$.error(e);
                    });
            }
        );
    },
    *watch(task) {
        yield task.watch(`${src}/**/*.js`, ["scripts"]);
        yield task.watch(`vendor/**/*.js`, ["vendor"]);
    }
};
